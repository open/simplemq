package main

import (
	"go.digitalcircle.com.br/open/simplemq/lib/smqcli"
	"log"
	"time"
)

func main() {
	c, err := smqcli.New("ws://localhost:8080/ws")
	if err != nil {
		panic(err.Error())
	}
	for {
		err = c.Pub("a", []byte("A:"+time.Now().String()))
		if err != nil {
			log.Printf(err.Error())
		}
		time.Sleep(time.Second)
	}
}
