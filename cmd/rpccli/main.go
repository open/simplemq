package main

import (
	"fmt"
	"go.digitalcircle.com.br/open/simplemq/lib/smqcli"
	"go.digitalcircle.com.br/open/simplemq/lib/types"
	"log"
	"runtime"
	"time"
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	c, err := smqcli.New("ws://localhost:8080/ws")
	if err != nil {
		panic(err.Error())
	}

	c.Sub("a", func(m *types.Msg) {
		ret := fmt.Sprintf("==> Got %s at %s", string(m.Payload), time.Now().String())
		log.Printf(ret)
		c.RpcReply(m, []byte(ret))
	})

	c.Sub("ab.*", func(m *types.Msg) {
		log.Printf("Got msg at %s: %s", m.Topic, string(m.Payload))
		c.RpcReply(m, []byte(fmt.Sprintf("Got msg at %s: %s", m.Topic, string(m.Payload))))
	})
	c.Sub("c.*", func(m *types.Msg) {
		//log.Printf("Got msg at %s: %s", m.Topic, string(m.Payload))
	})

	go func() {
		for {
			//bs, err := c.Rpc("ab01", []byte("A msg to AB01: "+time.Now().String()))
			//if err != nil {
			//	log.Printf(err.Error())
			//}
			//log.Printf(string(bs))
			//time.Sleep(time.Millisecond * 100)
		}
	}()

	go func() {
		for {
			c.Pub("ca", []byte("A msg to CA: "+time.Now().String()))
			//time.Sleep(time.Millisecond * 100)
		}
	}()

	go func() {
		for {
			c.Pub("cb", []byte("A msg to CB: "+time.Now().String()))
			//time.Sleep(time.Millisecond * 75)
		}
	}()

	go func() {
		for {
			c.Pub("cc", []byte("A msg to CC: "+time.Now().String()))
			//time.Sleep(time.Millisecond * 15)
		}
	}()

	go func() {
		for {
			c.Pub("cd", []byte("A msg to CD: "+time.Now().String()))
			//time.Sleep(time.Millisecond * 15)
		}
	}()
	go func() {
		for {
			c.Pub("ce", []byte("A msg to CE: "+time.Now().String()))
			//time.Sleep(time.Millisecond * 15)
		}
	}()
	go func() {
		for {
			c.Pub("cf", []byte("A msg to CF: "+time.Now().String()))
			//time.Sleep(time.Millisecond * 15)
		}
	}()

	for {
		//bs, err := c.Rpc("a", []byte("A Request"))
		//if err != nil {
		//	log.Printf(err.Error())
		//} else {
		//	log.Printf("Recv: %s", string(bs))
		//}
		time.Sleep(time.Minute)
	}
}
