package main

import (
	"go.digitalcircle.com.br/open/simplemq/lib/smqsvr"
	"net/http"
)

func main() {
	http.HandleFunc("/ws", smqsvr.Serve)
	http.HandleFunc("/stats", smqsvr.Stats)
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		panic(err)
	}
}
