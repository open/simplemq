package main

import (
	"go.digitalcircle.com.br/open/simplemq/lib/smqcli"
	"go.digitalcircle.com.br/open/simplemq/lib/types"
	"log"
	"time"
)

func main() {
	c, err := smqcli.New("ws://localhost:8080/ws")
	if err != nil {
		panic(err.Error())
	}
	c.Sub("a", func(m *types.Msg) {
		log.Printf(string(m.Payload))
	})
	for {
		time.Sleep(time.Second)
	}
}
