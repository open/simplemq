package main

import (
	"fmt"
	"go.digitalcircle.com.br/open/simplemq/lib/smqcli"
	"go.digitalcircle.com.br/open/simplemq/lib/types"
	"log"
	"time"
)

func main() {
	c, err := smqcli.New("ws://localhost:8080/ws")
	if err != nil {
		panic(err.Error())
	}
	c.Sub("a", func(m *types.Msg) {
		ret := fmt.Sprintf("Got %s at %s", string(m.Payload), time.Now().String())
		log.Printf(ret)
		c.RpcReply(m, []byte(ret))
	})
	for {
		time.Sleep(time.Second)
	}
}
