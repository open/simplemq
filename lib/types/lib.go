package types

import "encoding/json"

const (
	CMD_SUB      = "sub"
	CMD_UNSUB    = "unsub"
	CMD_PUB      = "pub"
	CMD_RPC      = "rpc"
	CMD_RPCREPLY = "rpcreply"
	CMD_CLOSE    = "close"
)

type Msg struct {
	Cmd     string            `json:"cmd,omitempty"`
	Id      string            `json:"id,omitempty"`
	ReplyTo string            `json:"reply_to,omitempty"`
	Topic   string            `json:"topic,omitempty"`
	Src     string            `json:"src,omitempty"`
	Header  map[string]string `json:"header,omitempty"`
	Payload []byte            `json:"payload,omitempty"`
}

func (m *Msg) Bytes() []byte {
	bs, _ := json.Marshal(m)
	return bs
}
