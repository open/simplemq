package random

import (
	"math/rand"
	"time"
	"unsafe"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const upperBytes = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
const letterNumsBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
const letterNumsUpperBytes = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

var src = rand.NewSource(time.Now().UnixNano())

func strFromSrc(n int, srcChars string) string {
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(srcChars) {
			b[i] = srcChars[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return *(*string)(unsafe.Pointer(&b))
}

func Str(n int) string {
	return strFromSrc(n, letterBytes)
}

func StrUpper(n int) string {
	return strFromSrc(n, upperBytes)
}

func StrLetterNum(n int) string {
	return strFromSrc(n, letterNumsBytes)
}
func StrLetterNumUpper(n int) string {
	return strFromSrc(n, letterNumsUpperBytes)
}
